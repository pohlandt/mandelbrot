package mandelbrot;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

public class Calculator implements Runnable {

	public static class Result {
		private final int x;
		private final int y;
		private final Color color;

		public Result(int x, int y, Color color) {
			this.x = x;
			this.y = y;
			this.color = color;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public Color getColor() {
			return color;
		}
	}

	private volatile ColorCalculator colorCalculator;
	private volatile Dimension panelSize;
	private volatile List<Result> result = new ArrayList<>();
	private volatile boolean complete = false;
	
	// [0] = x1, [1] = x2; [2] = y1, [3] = y2;
	private int[] _paintArea;
	
	// [0] = x1, [1] = x2; [2] = y1, [3] = y2;  
	private double[] _setBounds;
	
	public Calculator(ColorCalculator colorCalculator) {
		super();
		this.colorCalculator = colorCalculator;
	}

	public void reset(){
		complete = false;
		result.clear();
	}
	
	@Override
	public void run() {
		final var point = new double[2];
		for(int x = _paintArea[0]; x <= _paintArea[1]; x++){
			for(int y = _paintArea[2]; y <= _paintArea[3]; y++){
				setSetPoint(x, y, point);
				result.add(new Result(x, y, colorCalculator.getColor(point[0], point[1])));
			}
		}
		
		complete = true;
	}
	
	private void setSetPoint(int pixelX, int pixelY, double[] target) {
		target[0] = _setBounds[0] + ((_setBounds[1]  - _setBounds[0])
				/ ((double) panelSize.width)) * (pixelX - 1);

		target[1] = _setBounds[2] + ((_setBounds[3]  - _setBounds[2])
				/ ((double) panelSize.height)) * (pixelY - 1);
	}


	public ColorCalculator getColorCalculator() {
		return colorCalculator;
	}

	public void setColorCalculator(ColorCalculator colorCalculator) {
		this.colorCalculator = colorCalculator;
	}

	public double[] get_setBounds() {
		return _setBounds;
	}

	public void set_setBounds(double[] bounds) {
		_setBounds = bounds;
	}
	
	public int[] get_paintArea() {
		return _paintArea;
	}

	public void set_paintArea(int[] area) {
		_paintArea = area;
	}

	public Dimension getPanelSize() {
		return panelSize;
	}

	public void setPanelSize(Dimension size) {
		panelSize = size;
	}

	public List<Result> getResult() {
		return result;
	}

	public boolean isComplete() {
		return complete;
	}

}
