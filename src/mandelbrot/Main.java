package mandelbrot;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main implements Runnable {

    public static void main(String[] args) {
	    Runnable app = new Main();
        try {
            SwingUtilities.invokeAndWait(app);
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        MbTestFrame frame = new MbTestFrame();
        frame.setVisible(true);
    }
}
