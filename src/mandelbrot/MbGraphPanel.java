package mandelbrot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


public class MbGraphPanel extends JPanel {
	
	private final byte MAX_CALCULATOR_THREADS = 16;
	private final List<Calculator> _calculators = new ArrayList<>(MAX_CALCULATOR_THREADS);
	private final Set<Calculator> running = new HashSet<>(MAX_CALCULATOR_THREADS * 2);
	private final ExecutorService executor = Executors.newFixedThreadPool(MAX_CALCULATOR_THREADS);
	
	private double _x1;
	private double _x2;
	private double _y1;
	private double _y2;
	
	private ColorCalculator _colorCalculator;
	
	private boolean _multiThreaded = false; 
	
	private final static Font DEBUG_FONT = 
		new Font("Arial", Font.PLAIN, 48); 
	
	
	
	public MbGraphPanel(){
		this(-2, 1.5, -1.5, 1, 500);
	}
	
	public MbGraphPanel(double x1, double x2, double y1, double y2,
			int maxIterations){
		
		_x1 = x1;
		_x2 = x2;
		_y1 = y1;
		_y2 = y2;
		
		_colorCalculator = new ColorCalculator(maxIterations);
		
		setSize(200,200);
		setBackground(Color.BLACK);
				
		this.addMouseListener(new MyMouseListener());
		this.addMouseWheelListener(new MyMouseWheelListener());
		
		for (byte i = 1; i <= MAX_CALCULATOR_THREADS; i++){
			Calculator ct = new Calculator(new ColorCalculator(maxIterations));
			_calculators.add(ct);
		}
	}

	public void centerOnSet(double setX, double setY){
		double width = _x2 - _x1;
		double height = _y2 - _y1;
		
		_x1 = setX - width / 2;
		_x2 = setX + width / 2;
		_y1 = setY - height / 2;
		_y2 = setY + height / 2;
	}
	
	public void centerOnComponent(int x, int y){
		double[] setPoint = getSetPoint(x, y);
		centerOnSet(setPoint[0], setPoint[1]);
	}
	
	public void zoom(double factor){
		if (factor >= 1 || factor <= -1)
			throw new InvalidParameterException();
		
		double width = _x2 - _x1;
		double height = _y2 - _y1;
		
		_x1 += (width / 2) * factor;
		_x2 -= (width / 2) * factor;
		_y1 += (height / 2) * factor;
		_y2 -= (height / 2) * factor;
		
		this.repaint();
	}
	
	public void moveX(double factor){
		double width = _x2 - _x1;
		_x1 += width * factor;
		_x2 += width * factor;
				
		this.repaint();
	}
	
	public void moveY(double factor){
		double height = _y2 - _y1;
		_y1 += height * factor;
		_y2 += height * factor;
				
		this.repaint();
	}
		
	@Override
	public void setSize(int arg0, int arg1) {
		
		double aspectRatio = (_x2 - _x1) / (_y2 - _y1);
		if (arg0 != this.getWidth())
			super.setSize(arg0, (int) (arg0 * aspectRatio));
		else if(arg1 != this.getHeight())
			super.setSize((int) (arg1 / aspectRatio), arg1);
	}

	@Override
	public void paint(Graphics g) {
					
		Date before = new Date();
		
		if (_multiThreaded)
			paintMbSetThreaded(g);
		else
			paintMbSet(g);
	
		g.setColor(Color.RED);
		g.setFont(DEBUG_FONT);
	
		Date after = new Date();
		long ms = after.getTime() - before.getTime();
		
		String text = String.format("MaxIt: %1$d MSV: %2$.2f CS: %4$d ms: %3$d", 
				_colorCalculator.get_maxIterations(), 
				_colorCalculator.get_maxSquareValue(),
				ms,
				_colorCalculator.getStep());
		
		if (_multiThreaded)
			text += " MT";
		
		g.drawString(text, 1, 50);
	}
	
	private void paintMbSetThreaded(Graphics g){
		
		byte i = 1;
		int xSpan = this.getWidth() / _calculators.size();
		double[] setBounds = new double[]{_x1, _x2, _y1, _y2};

		running.clear();

		for (Calculator calculator : _calculators){
			calculator.reset();
			int x1 = (i-1) * xSpan;
			int x2 = (i == _calculators.size()) ? this.getWidth() : x1 + xSpan;
			
			calculator.set_paintArea(new int[]{x1, x2, 1, this.getHeight()});
			calculator.setPanelSize(this.getSize());
			calculator.set_setBounds(setBounds);

			executor.execute(calculator);

			running.add(calculator);

			i++;
		}
		

		while (!running.isEmpty()){
			var it = running.iterator();
			while (it.hasNext()){
				var calculator = it.next();
				if (calculator.isComplete()){
					for (var result : calculator.getResult()){
						g.setColor(result.getColor());
						g.fillRect(result.getX(), result.getY(), 1, 1);
					}
					it.remove();
				}
			}
		}
	}
	
	private void paintMbSet(Graphics g){
		double[] setPoint;
		
		for (int x = 1; x <= this.getWidth(); x++){
			for (int y = 1; y <= this.getHeight(); y++){
				setPoint = getSetPoint(x, y);
				g.setColor(_colorCalculator.getColor(setPoint[0], setPoint[1]));
				g.fillRect(x, y, 1, 1);
			}
		}		
	}

	private double[] getSetPoint(int pixelX, int pixelY) {
		double setX;
		setX = _x1 + ((_x2  - _x1) / ((double) this.getWidth())) * (pixelX - 1);
		double setY;
		setY = _y1 + ((_y2  - _y1) / ((double) this.getHeight())) * (pixelY - 1);
		
		return new double[]{setX, setY};
	}
	
	public int get_maxIterations() {
		return _colorCalculator.get_maxIterations();
	}

	public void set_maxIterations(int iterations) {
		_colorCalculator.set_maxIterations(iterations);
		
		for(Calculator ct : _calculators)
			ct.getColorCalculator().set_maxIterations(iterations);
		
		this.repaint();
	}
	
	public double get_maxSquareValue() {
		return _colorCalculator.get_maxSquareValue();
	}

	public int get_colorSteps() {
		return _colorCalculator.getStep();
	}

	public void set_colorSteps(int steps) {
		_colorCalculator.setStep(steps);
		
		for(Calculator ct : _calculators)
			ct.getColorCalculator().setStep(steps);
		
		this.repaint();
	}

	public void set_maxSquareValue(double squareValue) {
		_colorCalculator.set_maxSquareValue(squareValue);
		this.repaint();
	}
	
	private class MyMouseWheelListener implements MouseWheelListener{

		public void mouseWheelMoved(MouseWheelEvent e) {
			//centerOnComponent(e.getPoint().x, e.getPoint().y);
			double factor = -0.1 * e.getWheelRotation();
			zoom(factor);
		}
		
	}
	
	private class MyMouseListener implements MouseListener{

		public void mouseClicked(MouseEvent e) {
			centerOnComponent(e.getPoint().x, e.getPoint().y);
			MbGraphPanel.this.repaint();
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	
		
	}

	public boolean is_multiThreaded() {
		return _multiThreaded;
	}

	public void set_multiThreaded(boolean threaded) {
		_multiThreaded = threaded;
		this.repaint();
	}
	
	
}
