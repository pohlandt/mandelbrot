package mandelbrot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MbControlPanel extends JPanel {

	
	private MbGraphPanel _graphPanel;
	
	public MbControlPanel(MbGraphPanel graphPanel){
	
		_graphPanel = graphPanel;
		
		this.setLayout(new FlowLayout());
		JButton btn = new JButton("Close");
		btn.addActionListener(new ExitAction());
		this.add(btn);
		
		btn = new JButton("Zoom+");
		btn.addActionListener(new ZoomAction(0.1));
		this.add(btn);
		
		btn = new JButton("Zoom-");
		btn.addActionListener(new ZoomAction(-0.1));
		this.add(btn);
		
		btn = new JButton("Left");
		btn.addActionListener(new MoveXAction(-0.1));
		this.add(btn);
		
		btn = new JButton("Right");
		btn.addActionListener(new MoveXAction(0.1));
		this.add(btn);
		
		btn = new JButton("Up");
		btn.addActionListener(new MoveYAction(-0.1));
		this.add(btn);
		
		btn = new JButton("Down");
		btn.addActionListener(new MoveYAction(0.1));
		this.add(btn);
		
		btn = new JButton("Iter+");
		btn.addActionListener(new IterAction(5));
		this.add(btn);
		
		btn = new JButton("Iter-");
		btn.addActionListener(new IterAction(-5));
		this.add(btn);
		
		btn = new JButton("Iter++");
		btn.addActionListener(new IterAction(100));
		this.add(btn);
		
		btn = new JButton("Iter--");
		btn.addActionListener(new IterAction(-100));
		this.add(btn);
		
		btn = new JButton("Msv+");
		btn.addActionListener(new MsvAction(0.2));
		this.add(btn);
		
		btn = new JButton("Msv-");
		btn.addActionListener(new MsvAction(-0.2));
		this.add(btn);

		btn = new JButton("CS+");
		btn.addActionListener(new ColorStepsAction(1));
		this.add(btn);
		
		btn = new JButton("CS-");
		btn.addActionListener(new ColorStepsAction(-1));
		this.add(btn);
		
		btn = new JButton("Toggle MT");
		btn.addActionListener(new ToggleMTAction());
		this.add(btn);
	}
	
	private class ExitAction implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}			
	}
	
	private class ZoomAction implements ActionListener{

		double _factor;
		public ZoomAction(double factor){
			_factor = factor;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.zoom(_factor);
		}			
	}
	
	private class MoveXAction implements ActionListener{

		double _factor;
		public MoveXAction(double factor){
			_factor = factor;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.moveX(_factor);
		}			
	}
	
	private class MoveYAction implements ActionListener{

		double _factor;
		public MoveYAction(double factor){
			_factor = factor;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.moveY(_factor);
		}			
	}
	
	private class IterAction implements ActionListener{

		int _delta;
		public IterAction(int delta){
			_delta = delta;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.set_maxIterations(_graphPanel.get_maxIterations() + _delta);
		}			
	}

	private class ColorStepsAction implements ActionListener{

		int _delta;
		public ColorStepsAction(int delta){
			_delta = delta;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.set_colorSteps(_graphPanel.get_colorSteps() + _delta);
		}			
	}
	
	private class MsvAction implements ActionListener{

		double _factor;
		public MsvAction(double factor){
			_factor = factor;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			double old = _graphPanel.get_maxSquareValue();
			_graphPanel.set_maxSquareValue(old + old * _factor);
		}			
	}
	
	private class ToggleMTAction implements ActionListener{
		
		public ToggleMTAction(){
		}
		
		public void actionPerformed(ActionEvent arg0) {
			_graphPanel.set_multiThreaded(
					!_graphPanel.is_multiThreaded());
		}			
	}
}


