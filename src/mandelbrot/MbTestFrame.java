package mandelbrot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


public class MbTestFrame extends JFrame {

	public MbTestFrame(){
		super("MbTest");
		
		this.setSize(300, 300);
	
		this.addWindowListener(new MyWindowListener());
		
		this.setLayout(new BorderLayout());
		MbGraphPanel graphPanel =new MbGraphPanel();
		this.add(graphPanel, BorderLayout.CENTER);
		JPanel pnlControls = new MbControlPanel(graphPanel);
		pnlControls.setPreferredSize(new Dimension(200,50));
		this.add(pnlControls, BorderLayout.SOUTH);
		
	}
	
	private class MyWindowListener implements WindowListener{

		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowClosed(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowClosing(WindowEvent arg0) {
			System.exit(0);			
		}

		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		
	}
		
	
}
