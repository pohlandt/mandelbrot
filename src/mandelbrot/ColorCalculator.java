package mandelbrot;

import java.awt.*;


public class ColorCalculator {
	
	private int _maxIterations;
	private double _maxSquareValue = 4;
	private int _step = 8;

	private Color[] _colorArray;
	
	public ColorCalculator(int maxIterations) {
		super();
		set_maxIterations(maxIterations);
		fillColors();
	}

	private void fillColors(){
		_colorArray = new Color[256 / _step];
		for (int i = 0; i < _colorArray.length; i++) {
//			_colorArray[i] = new Color(i * step, (i * step) / 2, 255 - i * step);
			_colorArray[i] = new Color(i * _step, 0, 255);
		}
	}

	public int getStep(){
		return _step;
	}

	public void setStep(int step){
		if(_step != step){
			_step = step;
			fillColors();
		}
	}
	
	public Color getColor(double setX, double setY){
		int iterations = getIterations(setX, setY, 
				_maxSquareValue, _maxIterations);
		
		return _colorArray[iterations % _colorArray.length];
	}
	
	private int getIterations(double setX, double setY, 
			double maxSquareValue, int maxIterations){
		double squareValue = 0;
		int iteration = 0;
		double x = 0;
		double y = 0;
		
		while((squareValue <= maxSquareValue) 
				&& (iteration < maxIterations)){
			double xt = x * x - y * y + setX;
			double yt = 2 * x * y + setY;
			x = xt;
			y = yt;
			iteration++;
			squareValue = x * x + y * y;		
		}
		
		return iteration;
	}

	public int get_maxIterations() {
		return _maxIterations;
	}

	public void set_maxIterations(int iterations) {
		_maxIterations = iterations;			
	}

	public double get_maxSquareValue() {
		return _maxSquareValue;
	}

	public void set_maxSquareValue(double squareValue) {
		_maxSquareValue = squareValue;
	}
	
}
